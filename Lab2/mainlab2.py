# -*- coding: utf-8 -*-
'''
@file       mainlab2.py
@brief      Main file for Lab 2
@details    This contains all of the functions and the main code for Lab 2 and 
            is designed to be run on the Nucleo STM32 board with MicroPython. 
            This a program that lights the user LED on the Nucleo board at a 
            random time between 2 and 3 seconds, then measures the reaction 
            time of the user pressing the user button (blue) on the board. The 
            process is repeated until the user either does not respond for 1 second 
            after the LED is on, or if they press ctrl-c, and in either case 
            there is then an exit message displaying the average response time 
            and number of tries.
@author     Ben Sahlin
@date       January 21, 2021
'''

import utime
import pyb
import urandom
import micropython

#set up mem alloc to get callback function errors
micropython.alloc_emergency_exception_buf(200)

#vars for button interrupt callback
tries = 0
total_react_time = 0
react_time = 0

#set up user button interrupt function
def buttonInterrupt(pin):
    '''
    @brief          Callback function for button press
    @details        Used along with ExtInt object so that on button press, this 
                    callback function is called as an interrupt. On button press, 
                    this function records the reaction time based on a previously 
                    initialized timer, and prints the react time in timer counts 
                    to the console. It also adds the reaction time to the total 
                    reaction time and also tallies the number of tries, for 
                    later use in calculating the average reaction time.
    @param pin      Unused parameter pin, the pin that called the interrupt
    '''

    global total_react_time
    global tries
    global react_time
    react_time = response_timer.counter()
    total_react_time += react_time
    tries += 1
    print(react_time)
    pinA5.value(0)
    response_timer.deinit()

def exitProgram(timer):
    '''
    @brief          Exits lab 2 program
    @details        Is called when there is either no response from the user, 
                    or when the user presses ctrl-c. This function prints the 
                    exit message containing the average response time in timer 
                    counts as well as seconds, along with the number of tries. 
                    It also sets the user LED to off, disables the button 
                    interrupt so that the program can be run again, and 
                    denitializes the timer.
    @param timer    The timer object to be deinitialized.
    '''
    try:
        avg_react_time = int(total_react_time/tries)
    except ZeroDivisionError:
        #no tries recorded
        avg_react_time = 'N/A'
    print('Exiting.\nOver {:} tries, '
          'average reaction time was {:} timer counts = {:.4f} sec'
          .format(tries, avg_react_time, timCount2Sec(avg_react_time, timer)))
    #turn off LED
    pinA5.value(0)
    #disable extint object
    user_button_extint.disable()
    timer.deinit()
    
    
def timCB(called_timer):
    '''
    @brief                  Called when timer overflows
    @details                When this function is called, the user has not 
                            responded for 1 second based on the period and 
                            prescaler chosen for the timer in this lab. This 
                            function triggers the no user response situation.
    @param called_timer     This is the timer object that triggered the callback function.
    '''
    global no_user_response
    no_user_response = True
    print('Error: No user button press detected.')
    
def timCount2Sec(timCount, timer):
    '''
    @brief              Converts timer count to seconds
    @details            Uses timer frequency and period from timer object to 
                        convert the given timer count ticks to seconds.
    @param timCount     Integer timer count to convert to seconds.
    @param timer        Timer object that count is from, used to get conversion 
                        factor from counts to seconds.
    @returns t_sec      Time in seconds converted from timer count.
    '''
    t_sec= timCount/(timer.freq()*timer.period())
    return t_sec
    
#set up user button external interrupt object
user_button_extint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, buttonInterrupt)

#set max time to wait for user input as 1 sec
max_response_time = int(1e6)

#set up user LED
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
#set user LED to off
pinA5.value(0)

#set up no user response condition
no_user_response = False

print('When user LED lights, click blue user button.')
try:
    while no_user_response == False:
        #sleep for random time between 2 and 3 seconds
        utime.sleep_us(urandom.randint(int(2e6),int(3e6)))
        #turn on user LED
        pinA5.value(1)
        #set up timer to count response time, want it to hit period at 1 sec (max user response time)
        timer_clk_source = int(pyb.freq()[2])
        response_timer = pyb.Timer(2, period = timer_clk_source-1, prescaler = 0, callback = timCB)
        response_timer.counter(0)
        i = tries
        while i == tries:
            #waiting for user response to cause interrupt
            if no_user_response == True:
                exitProgram(response_timer)
                break           #quit second tier while loop
except KeyboardInterrupt:
    #ctrl-c pressed
    exitProgram(response_timer)
