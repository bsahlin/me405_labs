# -*- coding: utf-8 -*-
'''
@file       mainlab1.py
@brief      Main file for lab 01
@details    Contains Lab 01 vending machine finite state machine and UI
@author     Ben Sahlin
@date       January 11, 2021
'''

import keyboard

# Initialize code to run FSM (not the init state)
state = 0

def getChange(price, payment):
    '''
    @brief          Calculates transaction change
    @details        Calculates tuple of change given decimal value of price and 
                    tuple value of payment. Uses tuples of integer values of each 
                    denomination, of the form (pennies, nickels, dimes, 
                    quarters, ones, fives, tens, twenties). Designed for 
                    vending machine application.
    @param price    Float representing price of item, assumes two decimal places, additional digits truncated
    @param payment  Tuple representing payment in integer number of (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
    @returns        Returns None if insufficient funds, returns tuple representing change in integer number of (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
    '''
    #denominations in cents
    change_denoms = (1, 5, 10, 25, 100, 500, 1000, 2000)
    #first convert payment to integer value of cents
    payment_cents = 0
    for i in range(len(change_denoms)):
        payment_cents += payment[i]*change_denoms[i]
    #print('Payment: {:} cents'.format(str(payment_cents)))
    price_cents = int(price*100)
    change_cents = payment_cents - price_cents
    #print('Change: {:} cents'.format(str(change_cents)))

    if change_cents < 0:
        #payment insufficient
        return None        
    else:
        #payment is sufficient, calculate change
        #need a list because it's mutable
        change_list = [0]*len(change_denoms)
        #start at max denomination and go down to min
        for i in range(len(change_denoms)-1,-1,-1):
            change_list[i] = int(change_cents/change_denoms[i])
            #print('{:} x {:} given as change'.format(change_list[i], change_denoms[i]))
            change_cents = change_cents - change_list[i]*change_denoms[i]
            #print('Now {:} cents left as change'.format(change_cents))
        #return tuple representing change
        change_tuple = tuple(change_list)
        return change_tuple

def printWelcome():
    '''
    @brief  Prints Vendotron startup message.
    '''
    print('Vendotron starting. Insert coins using keys 0-7 and select drink using keys "c", "p", "s", "d". To eject coins press "e".')

def on_keypress(key):
    '''
    @brief      Callback function for keypress
    @details    Creates global variable pushed_key, a string that represents the key name
    @param key  Represents pushed key. But, don't need to specify parameters when calling a callback function
    '''
    global pushed_key
    pushed_key = key.name
    
def tuple2cents(money_tuple, change_denoms):
    '''
    @brief                  Converts tuple money value to cents value
    @details                Takes a tuple representation of money along with specified 
                            denomination values and converts to an integer value 
                            of cents. The two parameters must be formatted in the 
                            same order and same length so that the denomination values 
                            match the respective number of bills or coins for that 
                            denomination.
    @param money_tuple      Tuple representation of money value, where each entry 
                            is an integer representing the number of that denomination
                            of bills or coins.
    @param change_denoms    Tuple representation of monetary denominations, each 
                            entry is the integer value of cents of each denomination.
    @returns                An integer representing the number of cents that equal 
                            the inputted money value, calculated based on the 
                            inputted denominations.
    '''
    money_cents = 0
    for i in range(len(change_denoms)):
        money_cents += money_tuple[i]*change_denoms[i]
    return money_cents


# Implement FSM using while loop and an if statement
while True:
    # will run eternally until user presses CTRL-C
    if state == 0:
        ## State 0: Initialization
        # initializes FSM itself with all the code features already set up
        printWelcome()
        #denominations in cents
        change_denoms = (1, 5, 10, 25, 100, 500, 1000, 2000)
        #create list of inserted payment
        balance_list = [0]*len(change_denoms)
        #set up callback function
        pushed_key = None
        keyboard.on_press(on_keypress)
        print('Current Balance: {:.2f}'.format(tuple2cents(balance_list, change_denoms)/100))            
        state = 1   # on the next iteration, FSM will run state 1
        #set up beverage costs in cents with key being the first letter of the beverage
        bevcost_dict = {'c':100, 'p':120, 's':85, 'd':110}
        bevname_dict = {'c':'Cuke', 'p':'Popsi', 's':'Spryte', 'd':'Dr. Pupper'}
        
    elif state == 1:
        ## State 1: Display balance    
        #check for user input
        try:
            if pushed_key:
                if pushed_key.isdigit() == True:
                    if int(pushed_key) <= 7:
                        #valid coin or bill inserted
                        coin_index = int(pushed_key)
                        balance_list[coin_index] += 1
                        print('Current Balance: {:.2f}'.format(tuple2cents(balance_list, change_denoms)/100))                        
                    else:
                        #invalid coin/bill index
                        print('Invalid input')
                elif pushed_key == 'c' or pushed_key == 'p' or pushed_key == 's' or pushed_key == 'd':
                    #soda selection button pushed
                    selected_bev = pushed_key
                    if bevcost_dict[selected_bev] <= tuple2cents(balance_list, change_denoms):
                        #sufficient funds
                        print('Dispensing {:}'.format(bevname_dict[selected_bev]))
                        balance_list = list(getChange(bevcost_dict[selected_bev]/100, tuple(balance_list))) 
                        state = 2
                    else:
                        #insufficient funds
                        state = 3
                elif pushed_key == 'e':
                    state = 2
                pushed_key = None        
        except KeyboardInterrupt:
            break
            
    elif state == 2:
        ## State 2: Return balance
        #coin return button pushed
        print('Coins returned: {:}'.format(balance_list))
        #reset payment balance
        balance_list = [0]*len(change_denoms)
        printWelcome()
        print('Current Balance: {:.2f}'.format(tuple2cents(balance_list, change_denoms)/100))                
        state = 1
    elif state == 3:
        ## State 3: Insufficient Funds
        print('Insufficient funds.')
        print('Price of {:} is: {:.2f}'.format(bevname_dict[selected_bev], bevcost_dict[selected_bev]/100))
        state = 1
    else:
        print('Invalid state code')
        # this state shouldn't exist