## @file mainlab0.py
#  Brief doc for mainlab0.py
#
#  Detailed doc for mainlab0.py 
#
#  @author Ben Sahlin
#
#  @copyright License Info
#
#  @date January 9, 2021
#
#

import motor_example as motor

## A motor driver object
moto = motor.MotorDriver()

moto.set_duty_cycle(20)