## @file motor_example.py
#  Brief doc for motor_example.py
#
#  Detailed doc for motor_example.py 
#
#  @author Ben Sahlin
#
#  @copyright License Info
#
#  @date January 9, 2021
#
#  @package motor
#  Brief doc for the motor module
#
#  Detailed doc for the motor module
#
#  @author Ben Sahlin
#
#  @copyright License Info
#
#  @date January 9, 2021

import pyb

## A motor driver object
#
#  Details
#  @author Your Name
#  @copyright License Info
#  @date January 1, 1970
class Motor:

	## Constructor for motor driver
	#
	#  Detailed info on motor driver constructor
	def __init__(self):
		pass

	## Sets duty cycle for motor
	#
	#  Detailed info on motor driver duty cycle function
	#
	#  @param level The desired duty cycle
	def set_duty_cycle(self,level):
		pass