# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 15:37:31 2021

@author: CCHAN
"""

# import required modules 
import numpy as np 
import matplotlib.pyplot as plt 
from scipy import signal
from scipy.signal import butter, lfilter, freqs



def filter_order(f_sample, f_pass, f_stop, fs, wp, ws, Td, g_pass, g_stop):   
    # Conversion to prewrapped analog frequency 
    omega_p = (2/Td)*np.tan(wp/2) 
    omega_s = (2/Td)*np.tan(ws/2) 
      
    # Design of Filter using signal.buttord function 
    # N is the order 
    # Wn is the cut-off freq of the filter
    N, Wn = signal.buttord(omega_p, omega_s, g_pass, g_stop, analog=True) 
      
    return [N, Wn]
 
     
      
def butter_lowpass(cutOff, f_sample, order):
    nyq = 0.5 * fs
    normalCutoff = cutOff / nyq
    b, a = butter(order, normalCutoff, btype='low', analog = False) # not sure if analog should be true or false. should be digital if we have regular sample time so analog = false
    return b, a



def butter_lowpass_filter(data, cutoff, f_sample, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y



if __name__ == "__main__":
    ## Specifications of Filter 
    # sampling frequency 
    # the trick will be getting these filter parameters
    # how fast are we smpling the touchp panel for f_sample
    # assuming average sampling rate of 500us
    f_sample = 2000 
    # pass band frequency 
    # the upper and lower bounds of our filter for f_pass and f_stop
    f_pass = 4000  
    # stop band frequency 
    f_stop = 8000   
    # pass band ripple 
    fs = 0.5
    # pass band freq in radian 
    wp = f_pass/(f_sample/2)   
    # stop band freq in radian 
    ws = f_stop/(f_sample/2)  
    # Sampling Time 
    Td = 1  
     # pass band ripple [dB]
    g_pass = 0.5 
    # stop band attenuation [dB]
    g_stop = 40 
    
    Param = filter_order(f_sample, f_pass, f_stop, fs, wp, ws, Td, g_pass, g_stop)
    cutoff = Param[1]
    print('cutoff = ' + str(cutoff))
    order = Param[0]
    print('order = ' + str(order))
    # Demonstrate the use of the filter.
    # First make some data to be filtered.
    T = 5.0         # seconds
    n = int(T * fs) # total number of samples
    t = np.linspace(0, T, n, endpoint=False)
    # "Noisy" data.  We want to recover the 1.2 Hz signal from this.
    data = np.sin(1.2*2*np.pi*t) + 1.5*np.cos(9*2*np.pi*t) + 0.5*np.sin(12.0*2*np.pi*t)
    
    # Filter the data, and plot both the original and filtered signals.
    y = butter_lowpass_filter(data, cutoff, f_sample, order)
    
    plt.subplot(2, 1, 2)
    plt.plot(t, data, 'b-', label='data')
    plt.plot(t, y, 'g-', linewidth=2, label='filtered data')
    plt.xlabel('Time [sec]')
    plt.grid()
    plt.legend()
    
    plt.subplots_adjust(hspace=0.35)
    plt.show()