# -*- coding: utf-8 -*-
'''
@file       main9.py
@brief      The main file for the term project for ME 405
@details    This is the main file that runs the task to balance the platforms
            and then disables the motors once the task is ended. 

@author     Michelle Chandler
@author     Ben Sahlin
@date       March 10, 2020
'''

from EncoderDriver import EncoderDriver
from MotorDriver import MotorDriver
from TouchPanelDriver import TouchPanel
from StateSpaceController import StateSpaceController
from FSM_BalancePlatform import taskBalancePlatform
import pyb
from pyb import Pin
import utime

## Encoder Setup
#create pin objects for encoder 1
e1_ch1_pin = Pin(Pin.cpu.B6)
e1_ch2_pin = Pin(Pin.cpu.B7)
#create pin objects for encoder 2
e2_ch1_pin = Pin(Pin.cpu.C6)
e2_ch2_pin = Pin(Pin.cpu.C7)
#create timer object for encoder 1
tim4 = pyb.Timer(4)
#create timer object for encoder 2
tim8 = pyb.Timer(8)
#output shaft ticks/deg = 1000 cyc/rev * 4 ticks/cyc * 1 rev/360 deg)
ticks_per_deg = 1000*4/360
#create encoder objects for encoders 1 and 2
enc1 = EncoderDriver(e1_ch1_pin, e1_ch2_pin, tim4, ticks_per_deg)
enc2 = EncoderDriver(e2_ch1_pin, e2_ch2_pin, tim8, ticks_per_deg)

## Motor Setup
#sleep and fault pins
nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
nFAULT_pin = pyb.Pin(pyb.Pin.cpu.B2)

#pwm pins for motors 1 and 2
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)

tim = pyb.Timer(3, freq=20000)

#from motor datasheet
resistance = 2.21   #ohms
vdc = 12            #V
kt = 13.8/1000      #mNm/A*Nm/1000mNm = Nm/A torque constant=back emf constant

#motor driver object controls both motors
#Note: it is important to not create multiple MotorDriver objects because only the first one can have fault detection
moe = MotorDriver(nSLEEP_pin, nFAULT_pin, pin_IN1, pin_IN2, pin_IN3, pin_IN4, tim, resistance, vdc, kt)

## Touch Panel Setup
pin_xm = Pin(Pin.cpu.A0)
pin_xp = Pin(Pin.cpu.A1)
pin_ym = Pin(Pin.cpu.A6)
pin_yp = Pin(Pin.cpu.A7)
#specify panel width and height
panel_width = 182 #mm, x direction, from testing
panel_height = 107 #mm, y direction, from testing
#specify panel center
panel_center_x = 101 #mm, from testing
panel_center_y = 65 #mm, from testing
#create touch panel object
tp = TouchPanel(pin_xp, pin_xm, pin_yp, pin_ym, panel_width, panel_height, panel_center_x, panel_center_y)

## State Space Controller Setup
# gains from platform only matlab with Ts = .5 sec and OS = 10%
# k2 = -0.0225
# k4 = -0.4124
# gains from platform only matlab with Ts = .75 sec and OS = 5%
# k2 = -0.0132
# k4 = -0.1966
# now adjusted for damping = original damping * 5 and Ts = .75 sec and OS = 5%
# k2 = 0.0086
# k4 = -0.1966
# ball and platform with original damping, Ts = 5 sec and OS = 5%
# k1 = -0.0313
# k2 = -0.0268
# k3 = -0.1823
# k4 = -0.2540
# ball and platform with original damping, Ts = 3 sec and OS = 5%
# k1 = -0.1438
# k2 = -0.0525
# k3 = -0.3271
# k4 = -0.5433
# ball and platform with original damping, Ts = 3 sec and OS = 25%              # this one sort of works
# k1 = -0.1915
# k2 = -0.0547
# k3 = -0.6471
# k4 = -0.5707
# ball and platform with original damping, Ts = 2 sec and OS = 25%
# this one works pretty well, can put ball almost anywhere on the platform and it works
# gets messed up with high velocities and sometimes when the ball is all the way 
# on the corners of the touch panel
k1 = -0.6452
k2 = -0.1013
k3 = -2.6162
k4 = -1.2295

ssc = StateSpaceController(k1, k2, k3, k4)

run_interval = int(.01*1e6)
# create taskBalancePlatform object
taskBalancePlatform1 = taskBalancePlatform(enc1, enc2, moe, tp, ssc, run_interval)

# data logging stuff, can take out later to make it faster if we want to
# clear old datalog
file = open('datalog.csv', 'w')
file.write('')
file.close()
# create current log 
with open('datalog.csv','a') as file: # File to put data into
    file.write('Time [s], xd [m/s], yd [m/s], x [m], y[m], duty1, duty2\n') # Create file headings
    while True:
        try:
            taskBalancePlatform1.run()            
            # File write
            # file.write('{:},{:},{:},{:},{:},{:},{:}\n'
            #            .format(taskBalancePlatform1.current_time,
            #                    taskBalancePlatform1.xdot,
            #                    taskBalancePlatform1.ydot,
            #                    taskBalancePlatform1.x,
            #                    taskBalancePlatform1.y,
            #                    taskBalancePlatform1.duty_requested1,
            #                    taskBalancePlatform1.duty_requested2))
            # file.write('1\n')#str(taskBalancePlatform1.current_time)+','+str(taskBalancePlatform1.xdot)+','+str(taskBalancePlatform1.ydot)+','+str(taskBalancePlatform1.x)+','+str(taskBalancePlatform1.y)+','+str(taskBalancePlatform1.duty1)+','+str(taskBalancePlatform1.duty2)+'\n')
        except KeyboardInterrupt:
            taskBalancePlatform1.moe.disable()
            print('Balancing stopped by user')
            break


    
## Old testing code
# for i in range(100):
#     #manage run times and interval
#     utime.sleep_us(run_interval)
#     previous_time = current_time
#     current_time = utime.ticks_us()
#     actual_interval_secs = utime.ticks_diff(current_time, previous_time)/int(1e6) #actual interval between runs in seconds

#     #update encoder
#     thetay_ticks = enc1.update()                                #ticks
#     thetay = enc1.ticks2rad(thetay_ticks)                       #rad
#     thetayd_degs = enc1.calc_velocity(actual_interval_secs)     #deg/s
#     thetayd = thetayd_degs*3.1415/180                           #rad/s
#     print('enc1 thetay {:}, thetayd {:}'.format(thetay, thetayd))
    
#     #calculate desired duty cycle
#     torque_requested = ssc.calcTorque(thetayd, thetay)
#     duty_requested = moe.torque2duty(torque_requested)
#     print('motor 1 duty requested {:}'.format(duty_requested))
    
