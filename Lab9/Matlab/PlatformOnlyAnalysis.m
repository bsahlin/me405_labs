%Balance Platform No Ball
clc;
clear;
close all;

%from lab 6 with original damping,
% A22 = -3.1196;
% A24 = 52.7252;
% B21 = -571.9286;
%with damping = originaldamping*5
A22 = -15.5981;
A24 = 52.7252;
B21 = -571.9286;

%new platform only matrices
A = [A22, A24;
       1,   0];

B = [B21;
       0];

C = eye(2);
D = [0;0];

%create open loop system
states = {'thetaydot (rad/s)' 'thetay (rad)'};
inputs = {'Tx (Nm)'};
outputs = states;
sys_ss_ol = ss(A,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);

%decide on required characteristics and calculate required pole locations
% OS = .1; %choose percent overshoot
% Ts = .5; %s, choose settling time
OS = 0.05;
Ts = .75;

zeta = -log(OS)/sqrt(pi^2+log(OS)^2);
wn = 4/zeta/Ts;

Re = zeta*wn;           %magnitude real part of desired pole location
Im = wn*sqrt(1-zeta^2); %magnitude imaginary part of desired pole location
desired_poles = [-Re+Im*i; -Re-Im*i]

%desired_poles = [-.25+4i; -.25-4i];
%desired_poles = [-1.5+4i; -1.5-4i];

K = place(A,B,desired_poles)
%note at poles further left than about -1.5+-4i, gain K1 goes negative

%create closed loop system
A_cl = A-B*K;
sys_ss_cl = ss(A_cl,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);

%Case A- Equilibrium
X0 = [0, 0];
Ts = 10; %s
figure(1);
initial(sys_ss_cl,X0,Ts);
title('Case A- Equilibrium');

%Case C- 5 Degree Angled Platform
X0 = [0, deg2rad(10)];
figure(2);
Ts = 1; %s
initial(sys_ss_cl,X0,Ts);
title('Case C- 5 Degree Angled Platform');

