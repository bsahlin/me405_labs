# -*- coding: utf-8 -*-
'''
@file       StateSpaceController.py
@brief      Contains StateSpaceController class
@details    Used to manage full state feedback calculations with specified controller 
            gains and state space variables. Includes method for calculating requested 
            torque for the given state variable values.
@author     Michelle Chandler
@author     Ben Sahlin
@date       March 10, 2020
'''

class StateSpaceController:
    '''
    '''
    def __init__ (self, k1, k2, k3, k4):
        '''
        @brief      Creates StateSpaceController object
        @details    Creates a StateSpaceController object with four specified 
                    controller gain values.
        @param k1   Float representing full state feedback gain 1 which corresponds to xdot
        @param k2   Float representing full state feedback gain 2 which corresponds to thetadot
        @param k3   Float representing full state feedback gain 3 which corresponds to x
        @param k4   Float representing full state feedback gain 4 which corresponds to theta
        '''
        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.k4 = k4
        
    def calcTorque(self, xdot, thetaydot, x, thetay):
        '''
        @brief              Calculates torque requested
        @details            Uses previously defined contoller gains along with inputted 
                            state space variable values to calculate the requested motor 
                            torque.
        @param xdot         Float representing current value of xdot in m/s. This is ydot for 
                            the other motor and encoder pair.
        @param thetaydot    Float representing current value of thetaydot in rad/s. This is 
                            thetaxdot for the other motor and encoder pair.
        @param x            Float representing current value of x in m. This is y for 
                            the other motor and encoder pair.
        @param thetay       Float representing current value of thetay in rad. 
                            This is thetax for the other motor and encoder pair.
        '''
        # calculate torque requested
        Tx = -1*(self.k1*xdot+self.k2*thetaydot+self.k3*x+self.k4*thetay)
        return Tx
        
if __name__ == '__main__':
    # test code block
    # gains from platform only matlab with Ts = .5 sec and OS = 10%
    # k2 = -0.0225
    # k4 = -0.4124
    # gains from platform only matlab with Ts = 3 sec and OS = 5%
    k2 = 0.0008
    k4 = -0.0987
    ssc = StateSpaceController(k2, k4)
    print('torque is: {:}'.format(ssc.calcTorque(0,.08)))