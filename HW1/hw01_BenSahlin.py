# -*- coding: utf-8 -*-
'''
@file       hw01.py
@author     Ben Sahlin
@date       January 11, 2021
'''

def getChange(price, payment):
    '''
    @brief          Calculates transaction change
    @details        Calculates tuple of change given decimal value of price and 
                    tuple value of payment. Uses tuples of integer values of each 
                    denomination, of the form (pennies, nickels, dimes, 
                    quarters, ones, fives, tens, twenties). Designed for 
                    vending machine application.
    @param price    Float representing price of item, assumes two decimal places, additional digits truncated
    @param payment  Tuple representing integer number of (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
    @returns        Returns None if insufficient funds or invalid input, returns tuple representing change in integer number of (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
    '''
    #denominations in cents
    change_denoms = (1, 5, 10, 25, 100, 500, 1000, 2000)
    #check for valid payment and price inputs
    if price < 0:
        #price parameter is negative
        return None
    elif len(payment) != len(change_denoms):
        #wrong tuple length of payment
        return None
    for i in range(len(payment)):
        if payment[i] < 0:
            #negative number of coins/bills in payment parameter
            return None
    else:
        #valid payment and price inputs
        #first convert payment to integer value of cents
        payment_cents = 0
        for i in range(len(change_denoms)):
            payment_cents += payment[i]*change_denoms[i]
        #print('Payment: {:} cents'.format(str(payment_cents)))
        price_cents = int(price*100)
        change_cents = payment_cents - price_cents
        #print('Change: {:} cents'.format(str(change_cents)))
        if change_cents < 0:
            #payment insufficient
            return None        
        else:
            #payment is sufficient, calculate change
            #need a list because it's mutable
            change_list = [0]*len(change_denoms)
            #start at max denomination and go down to min
            for i in range(len(change_denoms)-1,-1,-1):
                change_list[i] = int(change_cents/change_denoms[i])
                #print('{:} x {:} given as change'.format(change_list[i], change_denoms[i]))
                change_cents = change_cents - change_list[i]*change_denoms[i]
                #print('Now {:} cents left as change'.format(change_cents))
            #return tuple representing change
            change_tuple = tuple(change_list)
            return change_tuple

if __name__ == '__main__':
    #test code runs when file runs by itself, not when imported
    apple_payment = (3, 0, 0, 2, 1, 0, 0, 2)
    apple_price = 15.59
    change_given = getChange(apple_price, apple_payment)
    print('Change given: {:}'.format(change_given))