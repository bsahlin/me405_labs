clc;
clear;
close all;
%Constants defined as variables for now, will be updated once parameters known
syms r_DAx r_DAy r_DAz
syms r_QDx r_QDy r_QDz
syms r_QOx r_QOy r_QOz
syms r_m l_r r_p l_p m_p g r_G F_f r_C m_B IP_O r_B
r_DA = [r_DAx;r_DAy;r_DAz]; %vector for D/A definition
r_QD = [r_QDx;r_QDy;r_QDz]; %vector for Q/D definition
r_QO = [r_QOx;r_QOy;r_QOz]; %vector for Q/O definition
r_m = 60    %mm
l_r = 50    %mm
r_p = 32.5  %mm
l_p = 110   %mm
m_p = 400   %g
g = 9.81/1000 %mm/s^2
r_G = 42    %mm
F_f = 1
r_C = 50    %mm
m_B = 30    %g
IP_O = 1    %parrallel axis might not work?
r_B = 10.5  %mm

syms r_DA r_QD r_QO
%Variables (17 of them)
syms v_D a_D v_Q a_Q thetad_A thetadd_A thetad_D thetadd_D
syms thetad_O thetadd_O theta_A theta_D theta_O X1_B T_motor 
syms thetadd_B aB_X1

%System of equations from hand calculations
eqns=[r_DA == [0;r_m*cos(theta_A);r_m*sin(theta_A)],
      r_QD == [0;-1*l_r*sin(theta_D);l_r*cos(theta_D)],
      r_QO == [-r_p*sin(theta_O)+l_p*cos(theta_O);0;r_p*cos(theta_O)+l_p*sin(theta_O)],
      v_D == cross([thetad_A;0;0], r_DA),                           %bar AD
      a_D == cross([thetadd_A;0;0], r_DA) - thetad_A^2 * r_DA,      %bar AD
      v_Q == v_D + cross([thetad_D;0;0],r_QD),                      %bar DQ
      a_Q == a_D + cross([thetadd_D;0;0],r_QD) - thetad_D^2 * r_QD, %bar DQ
      v_Q == cross([0;-1*thetad_O;0], r_QO)                         %bar OQ
      a_Q == cross([0;-1*thetadd_O;0], r_QO) - thetad_O^2 * r_QO,   %bar OQ


      m_p*g*r_G*sin(theta_O)+F_f*r_C-m_B*g*sin(theta_O)*X1_B+m_B*g*cos(theta_O)*r_C+T_motor/(r_m-l_r*theta_D)*cos(theta_O)*l_p-T_motor/(r_m-l_r*theta_D)*sin(theta_O)*r_p == IP_O*thetadd_O,
      
      m_B*g*theta_O*r_B == 2/5*m_B*r_B^2*thetadd_B+m_B*aB_X1*r_B,%part 3c
      sin(theta_O) == theta_O,    %small angle approx
      cos(theta_O) == 1,          %small angle approx
      sin(theta_D) == theta_D,    %small angle approx
      cos(theta_D) == 1,          %small angle approx
      sin(theta_A) == theta_A,    %small angle approx
      cos(theta_A) == 1];         %small angle approx
        
%the variables we want on the left hand side
vars = [thetadd_O,aB_X1];

%use equationsToMatrix function to put system of eqns in matrix form
[A,b] = equationsToMatrix(eqns,vars);
A
b