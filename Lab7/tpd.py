# -*- coding: utf-8 -*-
'''
@file       tpd.py
@brief      Contains TouchPanel class for resistive touch panel control
@details    Touch panel driver designed to work with resistive touch panel. Created 
            for use with the ME 405 Hardware Kit which includes the 4 wire resistive
            touch panel connected to the Nucleo STM32 board. Uses Micropython libraries.
            Touch panel 3 axis coordinate reading gathered in less than 1500 microseconds.
@author     Ben Sahlin
@date       Feb 25, 2021
'''

import pyb
from pyb import Pin
from pyb import ADC
import utime

class TouchPanel:
    def __init__(self, pin_xp, pin_xm, pin_yp, pin_ym, panel_width, panel_height, panel_center_x, panel_center_y):
        '''
        @brief      Creates TouchPanel object
        @details    Takes inputs for pins and panel dimensions, creates TouchPanel 
                    object designed to measure the location of a ball on a 
                                resistive touch panel.
        @param pin_xp           Pin object to use for positive X direction pin
        @param pin_xm           Pin object to use for negative x direction pin
        @param pin_yp           Pin object to use for positive y direction pin
        @param pin_ym           Pin object to use for negative y direction pin
        @param panel_width      Float representing width of touch panel in mm, corresponds to x direction
        @param panel_height     Float representing height of touch panel in mm, corresponds to y direction
        @param panel_center_x   Float representing X coordinate for panel center in mm
        @param panel_center_y   Float representing Y coordinate for panel center in mm
        '''
        #set debug to True to print additional debugging statements
        self.debug = True
        self.pin_xp = pin_xp
        self.pin_xm = pin_xm
        self.pin_yp = pin_yp
        self.pin_ym = pin_ym
        self.panel_width = panel_width
        self.panel_height = panel_height
        self.panel_center_x = panel_center_x
        self.panel_center_y = panel_center_y
        #include values chosen through testing
        self.z_sensitivity = 4000   #ADC reading cutoff for touch/not touch, chosen by testing
        self.adc_bounds_x = (210, 3780) #min and max readings respectively for x dir
        self.adc_bounds_y = (375, 3610) #min and max readings respectively for y dir
        print('TouchPanel object created')
    
    def getx(self):
        '''
        @brief      Gets X coordinate on panel
        @details    Get the X coordinate which corresponds to panel width direction,
                    with the origin located at the center of the panel. Note that 
                    this method does not check to see if there is an object touching 
                    the panel, it assumes contact.
        @return     Returs X coordinate as float in mm. Values range from -panel_width/2 to +panel_width/2.
        '''
        self.pin_xp.init(mode=Pin.OUT_PP, value = 1)    #set xp to high
        self.pin_xm.init(mode=Pin.OUT_PP, value = 0)    #set xm to low
        self.pin_yp.init(mode=Pin.ANALOG)               #float yp
        self.pin_ym.init(mode=Pin.IN)
        self.adc_ym = ADC(self.pin_ym)  #create ADC object to measure V on ym
        self.xpos = self.adc_ym.read()*self.panel_width/(self.adc_bounds_x[1]-self.adc_bounds_x[0])-self.panel_center_x
        return(self.xpos)
    
    def gety(self):
        '''
        @brief      Gets Y coordinate on panel
        @details    Get the Y coordinate which corresponds to panel height direction,
                    with the origin located at the center of the panel. Note that 
                    this method does not check to see if there is an object touching 
                    the panel, it assumes contact.
        @return     Returs Y coordinate as float in mm. Values range from -panel_height/2 to +panel_height/2.
        '''
        self.pin_yp.init(mode=Pin.OUT_PP, value = 1)    #set yp to high
        self.pin_ym.init(mode=Pin.OUT_PP, value = 0)    #set ym to low
        self.pin_xp.init(mode=Pin.ANALOG)               #float xp
        self.pin_xm.init(mode=Pin.IN)
        self.adc_xm = ADC(self.pin_xm)  #create ADC object to measure V on xm
        self.ypos = self.adc_xm.read()*self.panel_height/(self.adc_bounds_y[1]-self.adc_bounds_y[0])-self.panel_center_y
        return(self.ypos)
    
    def getz(self):
        '''
        @brief      Gets Z coordinate on panel
        @details    Get the Z coordinate which is a boolean that corresponds to 
                    whether or not there is an object touching the panel. The value 
                    is True if touching and False if not touching.
        @return     Returns Z coordinate boolean. True if touching, False if not.
        '''
        self.pin_yp.init(mode=Pin.OUT_PP, value = 1)    #set yp to high
        self.pin_xm.init(mode=Pin.OUT_PP, value = 0)    #set xm to low
        self.pin_xp.init(mode=Pin.ANALOG)               #float xp
        self.pin_ym.init(mode=Pin.IN)
        self.adc_ym = ADC(self.pin_ym)  #create ADC object to measure V on ym
        self.zV = self.adc_ym.read()
        #self.debugprint(str(self.zV))
        if self.zV >= self.z_sensitivity:
            self.zpos = False
        elif self.zV < self.z_sensitivity:
            self.zpos = True
        return(self.zpos)
    
    def getPos(self):
        '''
        @brief      Gets tuple representing object position
        @details    Gets X, Y, and Z positions using getX(), getY(), and getZ() 
                    methods. Updates the self.pos tuple in the form (X, Y, Z) to 
                    the most recent position found. X is in mm, Y is in mm, and 
                    Z is a boolean where True indicates touch detected and False 
                    indicates no touch detected. The origin for the coordinate 
                    system is located in the center of the panel. Also returns 
                    the self.pos tuple that was found.
        @return     Returns tuple containing (X, Y, Z), with corresponding data 
                    types (float, float, boolean) and units of (mm, mm, None).
        '''
        self.pos = (self.getx(), self.gety(), self.getz())
        return(self.pos)
       
    
    def debugprint(self,printstring):
        '''
        @brief      Tool for printing debugging messages
        @details    If variable debug is set to True, prints the string that is 
                    passed.
        @param printstring String to print
        '''
        if self.debug == True:
            print(printstring)
        
    def testGetPos(self, iterations):
        '''
        @brief              Tests position collection time
        @details            Tests to see if getPos() function meets lab requirements 
                            of collecting all coordinates in less than 1500 microseconds. 
                            The getPos() method is called a specified number of 
                            times and then the average run time of the method is 
                            calculated and returned.
        @param iterations   Integer number of iterations to run of getPos()
        @return             Returns float value of average run time of getPos() 
                            method over the specified number of iterations. The 
                            average run time is given in microseconds.
        '''
        self.time_list = [[None,None]]*iterations
        #collect times
        for i in range(iterations):
            self.time_list[i][0] = utime.ticks_us()
            self.getPos()
            self.time_list[i][1] = utime.ticks_us()
        #now calc average run time
        self.totalruntime = 0
        for i in range(iterations):
            self.totalruntime += utime.ticks_diff(self.time_list[i][1], self.time_list[i][0])
        self.avgruntime = self.totalruntime/iterations
        return(self.avgruntime)

if __name__ == '__main__':
    #test code
    #initialize Pin objects to pass to constructor
    pin_xm = Pin(Pin.cpu.A0)
    pin_xp = Pin(Pin.cpu.A1)
    pin_ym = Pin(Pin.cpu.A6)
    pin_yp = Pin(Pin.cpu.A7)
    #specify panel width and height
    panel_width = 182 #mm, x direction, from testing
    panel_height = 107 #mm, y direction, from testing
    #specify panel center
    panel_center_x = 101 #mm, from testing
    panel_center_y = 65 #mm, from testing
    #create touch panel object
    tp = TouchPanel(pin_xp, pin_xm, pin_yp, pin_ym, panel_width, panel_height, panel_center_x, panel_center_y)
    
    #test 100 times and average
    avg_time = tp.testGetPos(100)
    print('Average over 100 getPos is {:} microseconds'.format(avg_time))
    
    
    
    
    
    
    
    
    
    