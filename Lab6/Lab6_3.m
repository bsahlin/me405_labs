%Lab 06
%ME 405
%Ben Sahlin
%Feb 22, 2021

clc;
clear;
close all;
rm = 60/1000;           %m
lr = 50/1000;           %m
rB = 10.5/1000;         %m
rG = 42/1000;           %m
lP = 110/1000;          %m
rP = 32.5/1000;         %m
rC = 50/1000;           %m
mB = 30/1000;           %kg
mP = 400/1000;          %kg
IP = 1.88*10^6/1000^3;  %g-mm^2 * (m/1000mm)^2 * (kg/1000g) = kg-m^2
%b = 10/1000;            %mNm-s/rad * (Nm/1000 mNm) = Nm-s/rad
b = 50/1000;
g = 9.81;               %m/s^2
IB = 2/5*mB*rB^2;       %kg-m^2

syms x xd xdd thetay thetayd thetaydd Tx;
%define state vector X
X = [xd; thetayd; x; thetay];
%from lab 5 system of equations
M = [-(mB*rB^2+mB*rC*rB+IB)/rB, -(IB*rB+IP*rB+mB*rB^3+mB*rB*rC^2+2*mB*rB^2+mP*rB*rG^2+mB*rB*x^2)/rB;
     -(mB*rB^2+IB)/rB, -(mB*rB^3+mB*rC*rB^2+IB*rB)/rB];
f = [b*thetayd - g*mB*(sin(thetay)*(rB+rC)+x*cos(thetay))+Tx*lP/rm+2*mB*thetayd*x*xd-g*mP*rG*sin(thetay);
     -mB*rB*x*thetayd^2-g*mB*rB*sin(thetay)];
%find g matrix
g = [inv(M)*f;
     X(1:2,1)];
% g = [X(1:2,1);
%      inv(M)*f];
% Calculating Jacobian
syms xt(t) thetayt(t)
%substitute with time dependent variables to allow for differentiating
reg_vars = [x,xd,thetay,thetayd];
time_vars = [xt(t),diff(xt(t),t),thetayt(t),diff(thetayt(t),t)];
g = subs(g,reg_vars,time_vars);
%set up v for Jacobian matlab fcn
v = [diff(xt(t),t), diff(thetayt(t),t), xt(t), thetayt(t)];
%matlab jacobian fcn will not work with diff() in the v vector
Jx =[diff(g(1),v(1)), diff(g(1),v(2)), diff(g(1),v(3)), diff(g(1),v(4));
     diff(g(2),v(1)), diff(g(2),v(2)), diff(g(2),v(3)), diff(g(2),v(4));
     diff(g(3),v(1)), diff(g(3),v(2)), diff(g(3),v(3)), diff(g(3),v(4));
     diff(g(4),v(1)), diff(g(4),v(2)), diff(g(4),v(3)), diff(g(4),v(4))];

Ju = [diff(g(1),Tx);
      diff(g(2),Tx)
      diff(g(3),Tx)
      diff(g(4),Tx)];
%substitute back to non time dependent variables
Jx = subs(Jx,time_vars,reg_vars);
Ju = subs(Ju,time_vars,reg_vars);

%equilibrium point at
equilib_vars = [xd, thetayd, x, thetay, Tx];
equilib_vals = [0,0,0,0,0];
%evaluate Jacobian matrices at equilib pt
Jx = subs(Jx, equilib_vars, equilib_vals);
Ju = subs(Ju, equilib_vars, equilib_vals);
%use Jacobian matrices as A and B matrices for state space representation
A = double(Jx);
B = double(Ju);
%define C matrix to choose outputs (want all rows in X as outputs)
C = eye(4);
D = [0;0;0;0];

%Case A
X0 = [0, 0, 0, 0];
states = {'xdot (m/s)' 'thetaydot (rad/s)' 'x (m)' 'thetay (rad)'};
inputs = {'Tx (Nm)'};
outputs = states;
Ts = 1;
sys_ss = ss(A,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);
figure(1);
initial(sys_ss,X0,Ts);
title('Case A- Equilibrium');

%Case B- 5 cm ball offset
X0 = [0, 0, 0.05, 0];
sys_ss = ss(A,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);
Ts = 0.4; %s
figure(2);
initial(sys_ss,X0,Ts);
response_B = initial(sys_ss,X0,Ts);
title('Case B- 5cm Ball Offset');
xlim([0,0.4]);

%Case C
X0 = [0, 0, 0, deg2rad(5)];
sys_ss = ss(A,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);
figure(3);
Ts = 0.4; %s
initial(sys_ss,X0,Ts);
title('Case C- 5 Degree Angled Platform');
xlim([0,0.4]);

%Case D
X0 = [0, 0, 0, 0];
sys_ss = ss(A,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);
%define time vector and Tx input vector
t = 0:0.0001:0.4;
Tx = [[1/1000]*ones(1,5),zeros(1,length(t)-5)];
case4_response = lsim(sys_ss,Tx,t,X0);
figure(4);
%plot response with specified axis limits
subplot(4,1,1)
plot(t,case4_response(:,1));
title('Case D- 1 mNm-s Impulse');
%ylim([0,.5*10^-6]);
xlim([0,max(t)]);
ylabel('xdot (m/s)');
xlabel('Time (seconds)');

subplot(4,1,2)
plot(t,case4_response(:,2));
%ylim([0,.5*10^-5]);
xlim([0,max(t)]);
ylabel('thetaydot (r/s)');
xlabel('Time (seconds)');

subplot(4,1,3)
plot(t,case4_response(:,3));
%ylim([0,1*10^-7]);
xlim([0,max(t)]);
ylabel('x (m)');
xlabel('Time (seconds)');

subplot(4,1,4)
plot(t,case4_response(:,4));
% ylim([0,5*10^-4]);
xlim([0,max(t)]);
ylabel('thetay (rad)');
xlabel('Time (seconds)');

%Model Closed Loop System with Case B Inputs
X0 = [0, 0, .05, 0];
Ts = 20; %s
%from lab handout,
K = [-0.05, -0.02, -0.3, -0.2]; %[N-s, Nm-s, N, N-m] place(A,B,[-1000,-2+i,-2-i,0])
%Tx = -K*X
A_cl = A-B*K;
sys_cl = ss(A_cl,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);
figure(5);
t = 0:0.01:20;
u = zeros(1,length(t));
lsim(sys_cl,u,t,X0);
title('Closed Loop with Case B Inputs (5 cm Ball Offset)');

%make all plots bigger so that axis labels fit
for i = 1:5
   set(figure(i), 'Position', [100,100,800,600]);
end

%test pole zero maps
% figure(6)
% pzmap(sys_ss)
% figure(7)
% pzmap(sys_cl)
%ol_poles = pole(sys_ss)
%cl_poles = pole(sys_cl)