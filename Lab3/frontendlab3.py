# -*- coding: utf-8 -*-
'''
@file       frontendlab3.py
@brief      Front end UI for Lab 03.
@details    This file runs in the console on the PC. It waits for the user  
            keyboard input, 'g', and then sends that input over the serial port 
            to the Nucleo. It then waits for a response from the Nucleo that 
            contains the recorded data from a button press, and once that 
            data is recieved it is plotted. The recorded data from the button 
            press is also saved in a csv file.
@author     Ben Sahlin
@date       January 26, 2021
'''

import serial
import keyboard
import numpy as np
import matplotlib.pyplot as plt


def onKeypress(key):
    '''
    @brief      Callback function for keypress
    @details    Creates global variable pushed_key, a string that represents the key name.
    @param key  Represents pushed key. But, don't need to specify parameters when calling a callback function
    '''
    global pushed_key
    pushed_key = key.name
    
def plotData(input_array, response_array):
    '''
    @brief                  Plots input and response data
    @details                This function recieves two 2D Numpy arrays, each 
                            containing time values of seconds in the first column 
                            and voltage values in volts in the second column. Both 
                            of the arrays are plotted on the same axis.                            
    @param input_array      The input as a 2D Numpy array with time in the first 
                            column and voltage in the second. In Lab 3 this is a 
                            step response, which is what the button response would 
                            ideally look like if it did not require debouncing.
    @param response_array   The response data as a 2D Numpy array, with time in 
                            the first column and voltage in the second. In Lab 3 
                            this is the debounced button response, which appears 
                            to be a first order response.
    '''
    
    plt.plot(input_array[:,0], input_array[:,1], color='red')
    plt.plot(response_array[:,0], response_array[:,1], color='blue')#, marker=',', linestyle='None')
    plt.xlabel('Time (milliseconds)')
    plt.ylabel('Voltage (V)')
    plt.legend(['Recorded Button Response', 'Input Step'])
    plt.show()
    print('Spyder: Data plotted')

def saveCSV(response_array, filename):
    '''
    @brief                  Saves data as CSV file
    @details                Uses Numpy to save data of 2D array in a CSV file with 
                            a specified filename
    @param resonse_array    2D numpy array to save to CSV file
    @param filename         String containing desired filename including .csv
    '''
    np.savetxt(filename, (response_array[:,0], response_array[:,1]), delimiter = ',')
    print('Spyder: Data saved to '+filename)

#set up serial object for Nucleo communication
ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)

#set up keypress callback
pushed_key = None
keyboard.on_press(onKeypress)

data_array = np.zeros((500,2), dtype=float)
step_array = np.zeros((500,2), dtype=float)

print('Press \'g\' to enable data collection')
while True:
    try:
        if ser.in_waiting !=0:
            serial_input = ser.readline().decode('ascii').strip()
            print('Nucleo: ' + serial_input)
            try:
                #try to cast serial_input to list of i, time (microseconds), value
                i = int(serial_input.split(',')[0])
                t = int(serial_input.split(',')[1])
                v_output = int(serial_input.split(',')[2])
                v_input = int(serial_input.split(',')[3])
                #convert to units of miliseconds and voltage
                t = float(t/1e3)
                v_output = float(v_output*3.3/4095)
                v_input = float(v_input*3.3/4095)
                data_array[i] = [t,v_output]
                step_array[i] = [t,v_input]
                if i == len(data_array)-1:
                    print('Spyder: Data recieved from Nucleo')
                    #plot data
                    plotData(data_array, step_array)
                    #save data as csv
                    saveCSV(data_array,'lab3_button_response.csv')
                    print('Press \'g\' to enable data collection')
            except IndexError:
                #no comma split
                pass
            except ValueError:
                #not a data transfer
                pass
        elif pushed_key:
            print(pushed_key)
            if pushed_key == 'g':
                #send serial message to start data collection
                ser.write('g'.encode('ascii'))
            else:
                print('Spyder: Invalid input of {:}'.format(pushed_key))
            pushed_key = None
            
    except KeyboardInterrupt:
        ser.close()

