# -*- coding: utf-8 -*-
'''
@file       mainlab3.py
@brief      Main file for Lab 3 on Nucleo
@details    This is the main file for Lab 3 and is designed to be run on the 
            Nucleo STM32 board with MicroPython. Additionally, there is a jumper 
            connecting the user button pin, PC13, to the desired analog input pin, 
            A0. This allows the program to convert the analog button signal to 
            digital values. The goal of the lab is to record 
            a debounced response from the user button on the Nucleo. This is a 
            program that sets up initial variables and then waits for a user input 
            of 'g', which it recieves over serial communication from the PC console. Once it 
            recieves the user input to enable data collection, it begins continually 
            refilling a buffer of data points. The buffer is checked upon each fill
            to see if it contains a button response. If it does, the refilling is 
            stopped and the data is sent to the PC over the serial port. 
@author     Ben Sahlin
@date       January 26, 2021
'''
import pyb
import micropython
import array
from pyb import UART

#set up mem alloc to get callback function errors
micropython.alloc_emergency_exception_buf(200)

data_collected_string = ''

def buttonInterrupt(pin):
    '''
    @brief          Callback function for button press
    @details        Used along with ExtInt object so that on button press, this 
                    callback function is called as an interrupt. In this case, 
                    the user is just informed that the button press was recieved.
    @param pin      Unused parameter pin, the pin that called the interrupt
    '''
    global data_collected
    if data_collected == True:
        print('User button press occured. Data collected.')
    else:
        print('User button press occured. Data not collected. Press button again.')
        
def fillBuffer():
    '''
    @brief                  Continually fills buffer with button values
    @details                This function uses the button_adc object created previously to 
                            read the button count values, and then once the buffer 
                            is filled the recorded data is checked to see if the 
                            button response was contained within that buffer. If it was, 
                            then the function returns a value of True, and if not 
                            then it returns a value of False.
    @returns valid_data     A boolean representing whether the filled buffer is 
                            a valid step response (True) or not (False)
    '''
    global button_adc
    global response_counts_array
    global response_timer
    #collect response values
    button_adc.read_timed(response_counts_array, response_timer)
    #print('Collected unchecked data')
    valid_data = False
    #check if buffer contains valid data by seeing if it contains a value of 'pressed'
    #at beginning of array, and 'unpressed' at end
    if response_counts_array[-1] >= 4080 and response_counts_array[0] <= 10:
        valid_data = True
    return valid_data

def stepInputCreator(response_counts_val):
    '''
    @brief                      Adjusts step input for plotting
    @details                    Adjusts step input for plotting so that the 
                                input step matches the where button response 
                                begins to increase. Compares given response 
                                count to preset limits and returns either low (0)
                                or high (4095) value accordingly.
    @param response_counts_val  Response counts to compare to set limits.
    @returns                    Function returns 0 for a low button counts value and 
                                4095 for any other button counts value.
    '''
    if response_counts_val <= 10:
        return 0
    else:
        return 4095
    
        
#set up array to store collected response data
response_counts_array = array.array('H', (0 for index in range(500)))

#pin A0 is connected to C13 (user button)
#set up adc object for user button
button_adc = pyb.ADC(pyb.Pin.board.A0)

#set up UART
uart = UART(2)

try:
    #set up user button external interrupt object. Pull down bc want default value as LOW
    user_button_extint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, buttonInterrupt)
except ValueError as error:
    if str(error) == 'ExtInt vector 13 is already in use':
        #this occurs when rerunning program, just re-enable extint object
        user_button_extint.enable()

#set up timer object
response_timer = pyb.Timer(2, freq = 80e3)
#create time values array in microseconds
time_array = array.array('i', (int(i*1e6/response_timer.freq()) for i in range(len(response_counts_array))))

#start loop with button disabled and no data collected
user_button_extint.disable()
data_collected = False
try:
    while True:
        if data_collected == True:
            #disable button interrupt and send data to frontend
            user_button_extint.disable()
            for i in range(len(response_counts_array)):
                uart.write('{:},{:},{:},{:}\r\n'.format(i, time_array[i], response_counts_array[i], stepInputCreator(response_counts_array[i])))
            #reset for another data set to be collected
            data_collected = False     
        elif uart.any() != 0:
            user_input = uart.readline().decode('ascii')
            print(user_input)
            if user_input == 'g':
                #enable button to allow data collection     
                user_button_extint.enable()
                print('Click blue user button to generate step response. It may take multiple presses to generate valid response.')
                #now continually fill buffer until we find that the data contains a step
                data_collected == False
                while data_collected == False:
                    data_collected = fillBuffer()
                print('data collected')
            else:
                #there is input but it is invalid
                print('Invalid user input of ' + user_input)

except KeyboardInterrupt:
    #disable external interrupt object on ctrl-c exit from program
    user_button_extint.disable()
    print('User button external interrupt disabled')        
